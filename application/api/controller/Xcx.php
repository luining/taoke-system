<?php

namespace app\api\controller;

use think\Controller;
use ddj\Random;
use function Qiniu\json_decode;
class Xcx extends Controller
{
	private $appid = 'wx41c5eeadc4b8210d';
	private $secret = 'bb38a896b5f586bf20d5c8a968a0ec43';
	private $sessionKey;
	
	/**
	 * 小程序登录-修正版
	 */
	public function login()
	{
		if ($this->request->isPost()){
			$encryptedData = input('post.encryptedData');
			$iv = input('post.iv');
			$code = input('post.code');
			$step = (int)input('step',0);
			if (empty($code) || empty($iv) || empty($encryptedData)){
				$this->error('参数错误');
			}
			if ($step==0){
				//普通登录，即数据库中已经存在此用户信息
				$url = 'https://api.weixin.qq.com/sns/jscode2session?';
				$param = [
					'appid' => $this->appid,
					'secret' => $this->secret,
					'js_code' => $code,
					'grant_type' => 'authorization_code'
				];
				$re = http_get($url.http_build_query($param));
				$ret = json_decode($re,true);
				$this->sessionKey = $ret['session_key'] ?? '';
				if (empty($this->sessionKey)){
					$this->error('授权失败');
				}
				$wxinfo = self::decryptData($encryptedData, $iv);
				if (false!=$wxinfo){
					$uinfo = model('User')->where('unionid',$wxinfo['unionId'])->find();
					if ($uinfo){//登录
						if ($uinfo->status!=1){
							$this->error('您的账户涉嫌违规操作，已被禁用');
						}
						$uinfo->last_time = time();
						$uinfo->last_ip = request()->ip();
						$uinfo->login_count +=1;
						//换token,之前登录失效,小程序不挤掉
						//$token = md5(Random::uuid().time().Random::alnum(6));
						//$uinfo->token = $token;
						$uinfo->save();
						$re = [
							'token' => $uinfo['token'],
							'uid' => $uinfo['uid'],
							'pid' => $uinfo['pid'],
							'is_bind_mobile' => empty($uinfo['mobile']) ? 0 :1,
						];
						$this->success('登录成功','',$re);
					}else {
						//无此用户，跳到手机绑定页面,缓存session_key
						cache($iv.$code,$this->sessionKey);
						$this->result('',-1,'请绑定手机');
					}
				}else {
					$this->error('登录失败');
				}
				
			}else {
				//绑定手机号后的登录
				$User = model('User');
				$mobile = input('post.mobile');
				$smscode = input('post.smscode');
				if (empty($mobile)) $this->error('请输入手机号');
				if (empty($smscode)) $this->error('请输入短信验证码');
				//判断验证码是否正确
				$smsfind = model('Sms')->where('used',0)->where('type','bindmobile')->where('mobile',$mobile)->order('create_time desc')->find();
				if ($smsfind){
					$codearr = json_decode($smsfind['code'],true);
					if ($codearr['code']==$smscode){
						//时效性验证  15分内有效
						if ( (time()-$smsfind->getData('create_time')) > (15*60) ){
							$this->error('验证码已过期，请重新获取');
						}
						if (model('User')->get(['mobile'=>$mobile])){
							$this->error('此手机已被占用');
						}
						//创建用户
						$this->sessionKey = cache($iv.$code);
						$wxinfo = self::decryptData($encryptedData, $iv);
						if ($wxinfo==false){
							$this->error('登录失败');
						}
						$data = [
							'nickname' => $wxinfo['nickName'],
							'mobile' => $mobile,
							'token' => md5(Random::uuid().time().Random::alnum(6)),
							'reg_ip' => request()->ip(),
							'login_count' => 1,
							'invite_code' => make_invite_code(),
							'headimgurl' => $wxinfo['avatarUrl'],
							'province' => $wxinfo['province'],
							'city' => $wxinfo['city'],
							'sex' => $wxinfo['gender'],
							'unionid' => $wxinfo['unionId'],
						];
						//检测邀请记录
						$invite_logs = model('InviteLogs')->get(['mobile'=>$mobile]);
						if ($invite_logs){
							$data['pid'] = $invite_logs->uid;
							$invite_logs->is_bind = 1;
							$invite_logs->save();
						}
						//注册
						if ($User->save($data)){
							$re = [
								'token' => $data['token'],
								'uid' => $User->uid,
								'is_bind_mobile' => 1,
							];
							//邀请送积分
							if ($data['pid']>0){
								score_invite($data['pid']);
								//监听任务邀请粉丝
								add_task_score(10, $data['pid']);
								add_task_score(5, $data['pid']);
								// 发通知
								pushOffline($data['pid'],$data);
							}
							//使验证码失效
							$smsfind->used = 1;
							$smsfind->save();
							$this->success('注册成功','',$re);
						}else {
							$this->error('注册失败,请稍后再试~');
						}
					}else {
						$this->error('验证码错误');
					}
				}else {
					$this->error('验证码错误~');
				}
			}
		}else {
			$this->error('非法请求');
		}
		
		
		
	}
	/**
	 * 检验数据的真实性，并且获取解密后的明文.
	 * @param $encryptedData string 加密的用户数据
	 * @param $iv string 与用户数据一同返回的初始向量
	 * @param $data string 解密后的原文
	 *
	 * @return int 成功0，失败返回对应的错误码
	 */
	private function decryptData( $encryptedData, $iv )
	{
		if (strlen($this->sessionKey) != 24) {
			
			return false;
		}
		$aesKey=base64_decode($this->sessionKey);
		if (strlen($iv) != 24) {
			return false;
		}
		$aesIV=base64_decode($iv);
		
		$aesCipher=base64_decode($encryptedData);
		
		$result=openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
		
		$dataObj=json_decode( $result );
		if( $dataObj  == NULL )
		{
			return false;
		}
		if( $dataObj->watermark->appid != $this->appid )
		{
			return false;
		}
		return json_decode($result,true);
	}
    /**
     * 小程序登录
     */
	public function login2()
	{
		if ($this->request->isPost()){
			$headimgurl = input('post.avatarUrl');
			$nickname = input('post.nickname');
			$province = input('post.province');
			$city = input('post.city');
			$sex = input('post.gender');
			$step = input('step',0);
			if ($step==0){
				//普通登录，即数据库中已经存在此用户信息
				$code = input('post.code');
				if (empty($code)){
					$this->error('参数错误');
				}
				$url = 'https://api.weixin.qq.com/sns/jscode2session?';
				$param = [ 
					'appid' => 'wx41c5eeadc4b8210d',
					'secret' => 'bb38a896b5f586bf20d5c8a968a0ec43',
					'js_code' => $code,
					'grant_type' => 'authorization_code'
				];
				$re = http_get($url.http_build_query($param));
				$ret = json_decode($re,true);
				$unionid = $ret['unionid'] ?? '';
				if (empty($unionid)){
					$this->error('授权失败');
				}
				$uinfo = model('User')->where('unionid',$unionid)->find();
				if ($uinfo){//登录
					if ($uinfo->status!=1){
						$this->error('您的账户涉嫌违规操作，已被禁用');
					}
					$uinfo->last_time = time();
					$uinfo->last_ip = request()->ip();
					$uinfo->login_count +=1;
					//换token,之前登录失效
					$token = md5(Random::uuid().time().Random::alnum(6));
					$uinfo->token = $token;
					$uinfo->save();
					$re = [
						'token' => $uinfo['token'],
						'uid' => $uinfo['uid'],
						'pid' => $uinfo['pid'],
						'is_bind_mobile' => empty($uinfo['mobile']) ? 0 :1,
					];
					$this->success('登录成功','',$re);
				}else {
					//无此用户，跳到手机绑定页面
					$re = [
						'unionid' => $unionid,
					];
					$this->result($re,-1,'请绑定手机');
				}
				
			}else {
				$User = model('User');
				//+手机绑定注册
				$unionid = input('post.unionid');
				$mobile = input('post.mobile');
				$smscode = input('post.smscode');
				if (empty($unionid)) $this->error('参数错误');
				if (empty($mobile)) $this->error('请输入手机号');
				if (empty($smscode)) $this->error('请输入短信验证码');
				//判断验证码是否正确
				$smsfind = model('Sms')->where('used',0)->where('type','bindmobile')->where('mobile',$mobile)->order('create_time desc')->find();
				if ($smsfind){
					$codearr = json_decode($smsfind['code'],true);
					if ($codearr['code']==$smscode){
						//时效性验证  15分内有效
						if ( (time()-$smsfind->getData('create_time')) > (15*60) ){
							$this->error('验证码已过期，请重新获取');
						}
						//创建用户
						$data = [
							'nickname' => $nickname,
							'mobile' => $mobile,
							'token' => md5(Random::uuid().time().Random::alnum(6)),
							'reg_ip' => request()->ip(),
							'login_count' => 1,
							'invite_code' => make_invite_code(),
							'headimgurl' => $headimgurl,
							'province' => $province,
							'city' => $city,
							'sex' => $sex,
							'unionid' => $unionid,
						];
						//检测邀请记录
						$invite_logs = model('InviteLogs')->get(['mobile'=>$mobile]);
						if ($invite_logs){
							$data['pid'] = $invite_logs->uid;
							$invite_logs->is_bind = 1;
							$invite_logs->save();
						}
						//注册
						if ($User->save($data)){
							$re = [
								'token' => $data['token'],
								'uid' => $User->uid,
								'is_bind_mobile' => 1,
							];
							//邀请送积分
							if ($data['pid']>0){
								score_invite($data['pid']);
								//监听任务邀请粉丝
								add_task_score(10, $data['pid']);
								add_task_score(5, $data['pid']);
								// 发通知
								pushOffline($data['pid'],$data);
							}
							//使验证码失效
							$smsfind->used = 1;
							$smsfind->save();
							$this->success('注册成功','',$re);
						}else {
							$this->error('注册失败,请稍后再试~');
						}
					}else {
						$this->error('验证码错误');
					}
				}else {
					$this->error('验证码错误~');
				}
				
			}
		}else {
			$this->error('非法请求');
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
