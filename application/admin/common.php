<?php
function _import($section_id,$goods,$default_cid=[1,2,3,4,5,6,7,8,9,10])
{
    $item_ids = [];
    foreach ($goods as $k=>$v){
        if ($section_id==0){//自动识别
            if (!in_array($v['cid'], $default_cid)){
                unset($goods[$k]);//不导入
            }else {
                $goods[$k]['section_id'] = $v['cid'];
                $item_ids[] = $v['item_id'];
            }
        }else {
            $goods[$k]['section_id'] = $section_id;
            $item_ids[] = $v['item_id'];
        }
        unset($goods[$k]['id']);
    }
    //删除商品库里的产品
    if (db('goods')->whereIn('item_id', $item_ids)->delete()!==false){
        db('goods')->strict(false)->insertAll($goods);
        //删除采集库
        db('goods_collect')->whereIn('item_id', $item_ids)->delete();
    }
}
//版块
function get_section_names()
{
    return db('GoodsSection')->where('status','neq',-1)->cache('section_id_2_name')->column('name','id');
}