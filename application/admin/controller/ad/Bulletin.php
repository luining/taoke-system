<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller\ad;
use app\common\controller\Admin;
use think\cache\driver\Redis;
use think\Db;
use think\facade\Cache;
/**
* 后台设置控制器
*/
class Bulletin extends Admin
{

    public function initialize()
    {
        parent::initialize();
    }


    /**
     * 获取配置
     * @return mixed
     */
    public function index()
    {

        $ad_bulletin = get_db_config(true)['ad_bulletin'];

        $img = $ad_bulletin['bulletin_list'][0]['content'];
        $is_auto_popup = $ad_bulletin['is_auto_popup'];

        $this->assign('img',$img);
        $this->assign('is_auto_popup',$is_auto_popup);

        var_dump($ad_bulletin);
        return $this->fetch();
    }

}