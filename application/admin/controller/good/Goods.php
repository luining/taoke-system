<?php

namespace app\admin\controller\good;

use think\Controller;
use app\common\controller\Admin;

class Goods extends Admin
{
    protected $model;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('Goods');
    }
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $map = [];
            //版块
            $section_id = intval(input('section_id'));
            if ($section_id){
                $map[] = ['section_id','=',$section_id];
            }
            //关键词
            //智能识别关键词
            $keyword = input('keyword');
            if (!empty($keyword)){
                if (is_numeric($keyword)){
                    $map[] = ['item_id','eq',$keyword];
                }else {
                    $map[] = ['title','like',"%{$keyword}%"];
                }
            }
            
            $list = $this->model
            ->where($map)
            ->order('id desc')
            ->paginate(input('limit',15));
            $list->append(['is_tmall_text','section_id_text']);
            //返回layui分页
            return json(layui_page($list));
        }
        $this->assign('sections',get_section_names());
        return $this->fetch();
    }
    /**
     * 删除
     */
    public function del($ids='')
    {
        $type = input('type');
        if ($type=='all'){
            $count = db('goods')->delete(true);
            if ($count){
                $this->success('删除成功');
            }else {
                $this->error('删除失败');
            }
        }else {
            if ($ids){
                $count = $this->model->where('id','in',$ids)->delete();
                if ($count){
                    $this->success('删除成功');
                }else {
                    $this->error('删除失败');
                }
            }else{
                $this->error('请选择您要操作的数据');
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
