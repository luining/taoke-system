<?php

namespace app\admin\controller\good;

use think\Controller;
use app\common\controller\Admin;

class Section extends Admin
{
    protected $model;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('GoodsSection');
    }
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $list = $this->model
            ->where('status','neq',-1)
            ->order('sort desc,id asc')
            ->paginate(input('limit',15));
            $list->append(['is_jinping_text','user_type_text','is_index_text']);
            //返回layui分页
            return json(layui_page($list));
        }
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {
        //
        return $this->edit();
    }


    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id='')
    {
        if (IS_AJAX){
            cache('section_id_2_name',null);//清除缓存
            $data = input('post.');
            if (empty($id)){//添加
                model('GoodsSection')->allowField(true)->save($data);
                $this->success('添加成功');
            }else {
                $this->model->allowField(true)->save($data,['id'=>$id]);
                
                $this->success('编辑成功');
            }
        }else {
            
            $info = $this->model->get($id);
            $this->assign('info',$info);
            return $this->fetch('edit');
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($ids='')
    {
        if (is_array($ids)){
            if (count(array_intersect([1,2,3,4,5,6,7,8,9,10],$ids))>0){
                $this->error('此版块不能支持删除');
            }
        }else {
            if ($ids>1&&$ids<11){
                $this->error('此版块不能支持删除');
            }
        }
        parent::del($ids);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
