<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\common\controller;
use app\admin\library\Auth;
use think\Controller;
//load_trait('library/traits/Admin');
/**
 * 后台控制器基类
 */
class Admin extends Controller
{

    /**
     * 返回码,默认为null,当设置了该值后将输出json数据
     * @var int
     */
    protected $code = null;

    /**
     * 返回内容,默认为null,当设置了该值后将输出json数据
     * @var mixed
     */
    protected $data = null;

    /**
     * 返回文本,默认为空
     * @var mixed
     */
    protected $msg = '';

    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $noNeedRight = [];
    /**
     * 权限控制类
     * @var Auth
     */
    protected $auth = null;
    /**
     * 系统配置
     */
    public $config;
    /**
     * 引入后台控制器的traits
     */
    //use \app\admin\library\traits\Admin;

    public function initialize()
    {
        $modulename = $this->request->module();
        $controllername = strtolower($this->request->controller());
        $actionname = strtolower($this->request->action());

        $path = '/' . $modulename . '/' .  $controllername . '/' . $actionname;
        
        // 定义是否AJAX请求
        !defined('IS_AJAX') && define('IS_AJAX', $this->request->isAjax());


        $this->auth = Auth::instance();
        
        // 设置当前请求的URI
        $this->auth->setRequestUri($path);
        // 检测是否需要验证登录
        if (!$this->auth->match($this->noNeedLogin))
        {
            //检测是否登录
            if (!$this->auth->isLogin())
            {
                $this->redirect('index/login');
            }
            // 判断是否需要验证权限
            if (!$this->auth->match($this->noNeedRight))
            {
                // 判断控制器和方法判断是否有对应权限
                $path = $this->request->path();
                $path = substr($path, 0, 1) == '/' ? $path : '/' . $path;
                if (!$this->auth->check($path))
                {
                    $this->result('',-1,'无访问权限','json');
                }
            }
        }

        // 设置面包屑导航数据
        $breadcrumb = $this->auth->getBreadCrumb($path);

        // 配置信息
        $config = [
            'site'           => config("site."),
            //'upload'         => Configvalue::upload(),
            'modulename'     => $modulename,
            'controllername' => $controllername,
            'actionname'     => $actionname,
            'moduleurl'      => url("/{$modulename}", '', false),
            //'referer'        => Session::get("referer")
        ];
        //数据库里的配置
        $db_config = get_db_config(true);
        $this->config = array_merge($config,$db_config);
        $this->assign('site', config("site."));
        $this->assign('config', $this->config);
        $this->assign('admin', session('admin'));
    }
    /**
     * 删除
     */
    public function del($ids = "")
    {
        if ($ids){
            $data = [
                'delete_time' => time(),
                'status' => -1,
                'delete_uid' => $this->auth->uid,
            ];
            $count = $this->model->allowField(true)->save($data,[['id','in',$ids]]);
            if ($count){
                $this->success('删除成功');
            }else {
                $this->error('删除失败');
            }
        }else{
            $this->error('请选择您要操作的数据');
        }
        
        return;
    }

    /**
     * 析构方法
     *
     */
   /*  public function __destruct()
    {
        //判断是否设置code值,如果有则变动response对象的正文
        if (!is_null($this->code)){
            //$this->result($this->data, $this->code, $this->msg, 'json');
            if (IS_AJAX)
                header('Content-type:application/json');
            echo json_encode(['code'=>$this->code,'msg'=>$this->msg,'data'=>$this->data],JSON_UNESCAPED_UNICODE);
            exit();
        }
    } */

}
