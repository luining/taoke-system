<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\common\model;
use think\Model;
class MoneyLogs extends Model
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;
    /**
     * 添加、减少金额
     */
    public function add_money($data)
    {
        if (empty ( $data ) || empty ($data['type']) || empty($data['money']) || empty($data['uid']))
            return false;
        //变化前余额
        $before_blance = db('User')->where('uid',$data['uid'])->value('money');
        if ($before_blance===false)
            return false;
        $data['blance'] = $before_blance+$data['money'];
        $data['create_time'] = time();
        //添加
        if (db('MoneyLogs')->insert($data))
        {
            //更新总余额    避免并发情况下查询延迟出现错误
            //$agent->where('id',$data['agent_id'])->setField('blance',$data['blance']);
            db('User')->where('uid',$data['uid'])->setInc('money',$data['money']);
        }
        else {
            return false;
        }
        return true;
    }
    public function getTypeAttr($value)
    {
    	$text_arr = config('site.money_type');
    	return $text_arr[$value];
    }
    
    
}