<?php /*a:2:{s:80:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/good/section/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<a href="javascript:location.reload();" class="layui-btn layui-btn-sm"><i class="layui-icon">&#x1002;</i></a>
	<a href="javascript:;" class="layui-btn layui-btn-sm" id="add">
		<i class="layui-icon">&#xe608;</i> 添加
	</a>
	<button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
		<i class="layui-icon">&#xe640;</i> 删除
	</button>
	<form class="layui-form form1" action="<?php echo url('set/edit'); ?>" style="display: inline-block;margin-left: 50px;" _lpchecked="1">
		商品默认排序
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 150px;">
				<select name="config[app_goods_default_sort]" lay-verify="required">
					<option></option>
					<optgroup label="按添加时间"></optgroup>
					<option value="id desc" selected="">按添加时间排前</option>
					<option value="id asc">按添加时间排后</option>
					<optgroup label="按销量"></optgroup>
					<option value="sales desc">销量从高到低</option>
					<option value="sales asc">销量从低到高</option>
					<optgroup label="按价格-券后价"></optgroup>
					<option value="price_after_quan desc">价格从高到低</option>
					<option value="price_after_quan asc">价格从低到高</option>
					<optgroup label="佣金比例"></optgroup>
					<option value="rate desc">佣金从高到低</option>
					<option value="rate asc">佣金从低到高</option>
					<optgroup label="按券开始时间"></optgroup>
					<option value="quan_start_time desc">按券开始时间排前</option>
					<option value="quan_start_time asc">按券开始时间排后</option>
					<optgroup label="随机"></optgroup>
					<option value="rand()">随机排序</option>
		        </select>
			</div>
		</div>
		 <div class="layui-inline">
		 	<div class="layui-input-inline">
		 		<button class="layui-btn layui-btn-sm" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form1">保存</button>
		 	</div>
		 </div>
	</form>
</blockquote>
<table id="tb1" lay-filter="_tb1"></table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.id }}">
</script>
<script type="text/html" id="bar">
	<div class="layui-btn-group">
	  <a class="layui-btn layui-btn-xs edit" data-url="<?php echo url('edit'); ?>?id={{ d.id }}" >编辑</a>
	  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('del'); ?>?ids={{ d.id }}" >删除</a>
	</div>
</script>
<script>
	layui.config({
		base: '/static/js/',
	});
	layui.use(['tool'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool;
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('index'); ?>',
			limit:15,
			limits:[10,15,20,50,100],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-100',
			cols:[[
				{title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>',fixed:'left',templet:'#check',width:50},
				{title:'id',field:'id',width:50},
				{title:'版块名称',field:'name'},
				{title:'过滤销量',field:'filter_volume'},
				{title:'过滤佣金',field:'filter_yj'},
				{title:'精品推荐',field:'is_jinping_text'},
				{title:'首页显示',field:'is_index_text'},
				{title:'店铺类型',field:'user_type_text'},
				{title:'排序',field:'sort'},
				{title:'操作',fixed: 'right', width:150, align:'center', templet: '#bar'}

			]]
		});
		//添加
		$(document).on('click','#add,.edit',function(){
		    var url = '<?php echo url('add'); ?>',title = '添加版块';
		    if($(this).hasClass('edit')){
		      url = $(this).data('url');
		      title = '编辑版块';
		    }
		  	layer.open({
		      title:title,
		      type: 2,
		      area: ['50%', '80%'],
		      fixed: false, //不固定
		      maxmin: true,
		      content: url,
		      shade:0,
		      id:'group_add'
		    });
		});
		tool.setValue('config[app_goods_default_sort]','<?php echo htmlentities($config['app_goods_default_sort']); ?>')
	});
</script>

</html>