<?php /*a:2:{s:79:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/count/index/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
<style>
.info-box {
    height: 85px;
    background-color: white;
    background-color: #ecf0f5;
}

.info-box .info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 85px;
    width: 85px;
    text-align: center;
    font-size: 45px;
    line-height: 85px;
    background: rgba(0, 0, 0, 0.2);
}

.info-box .info-box-content {
    padding: 5px 10px;
    margin-left: 85px;
}

.info-box .info-box-content .info-box-text {
    display: block;
    font-size: 14px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    text-transform: uppercase;
}

.info-box .info-box-content .info-box-number {
    display: block;
    font-weight: bold;
    font-size: 18px;
}

.major {
    font-weight: 10px;
    color: #01AAED;
}

.main {
    margin-top: 15px;
}

.main .layui-row {
    margin: 10px 0;
}
.row2 .info-box p{text-align: center;}
.row2 .info-box p:first-child{font-size: 20px;font-weight: bold;padding-top:20px;}
</style>

</head>

<body>
	<div class="admin-body">
		
<div class="main">
	<div class="layui-row layui-col-space15">
	    <div class="layui-col-md6">
	        <div class="info-box">
	            <span class="info-box-icon" style="background-color:#00c0ef !important;color:white;"><i class="fa fa-jpy" aria-hidden="true"></i></span>
	            <div class="info-box-content">
	                <span class="info-box-text">未提现余额</span>
	                <span class="info-box-number">￥<?php echo htmlentities($count['total_money']); ?></span>
	            </div>
	        </div>
	    </div>
	    <div class="layui-col-md6">
	        <div class="info-box">
	            <span class="info-box-icon" style="background-color:#dd4b39 !important;color:white;"><i class="fa fa-money" aria-hidden="true"></i></span>
	            <div class="info-box-content">
	                <span class="info-box-text">累积订单</span>
	                <span class="info-box-number">￥<?php echo htmlentities($count['total_orders']); ?></span>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="layui-row layui-col-space15 row2">
		<div class="layui-col-md4">
			<div class="info-box" style="text-align: center;">
				<p></p>
				<form class="layui-form search" action="" style="display: inline-block;">
					<div class="layui-inline">
						<div class="layui-input-inline">
							<input type="text" name="date_start" value="<?php echo date('Y-m-d',$date_start); ?>" autocomplete="off" class="layui-input" id="date">
						</div>
					</div>
					 <div class="layui-inline">
					 	<div class="layui-input-inline">
					 		<button class="layui-btn layui-btn-sm sbtn" lay-submit="" lay-filter="searchsub" id="search"><i class="layui-icon"></i> 搜索</button>
					 	</div>
					 </div>
				</form>
			</div>
		</div>

		<div class="layui-col-md4">
			<div class="info-box">
				<p>￥<?php echo htmlentities($count['total_draw']); ?></p>
				<p>余额提现</p>
			</div>
		</div>
		<div class="layui-col-md4">
			<div class="info-box">
				<p>￥<?php echo htmlentities($count['total_rj']); ?></p>
				<p>订单佣金</p>
			</div>
		</div>
	</div>
</div>


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script>
layui.use('laydate', function(){
  var laydate = layui.laydate;
  
  //执行一个laydate实例
  laydate.render({
    elem: '#date' //指定元素
    ,max: '<?php echo date('Y-m-d'); ?>'
  });
});
</script>

</html>