<?php /*a:2:{s:80:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/order/taobao/index.html";i:1547509564;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<a href="javascript:location.reload();" class="layui-btn layui-btn-sm"><i class="layui-icon">&#x1002;</i></a>
	<button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
		<i class="layui-icon">&#xe640;</i> 删除
	</button>
	<form class="layui-form search" action="" style="display: inline-block;float: right;" _lpchecked="1">
		<div class="layui-input-inline" style="width: 300px;">
			<input type="text" name="date" class="layui-input" id="date" value="<?php echo date('Y-m-d 00:00:00',time()); ?> ~ <?php echo date('Y-m-d 23:59:59',time()); ?>">
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 120px;">
				<select name="order_status" >
			        <option value="">订单状态</option>
			        <?php $_result=config('site.orderst_text');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if($key > '0'): ?>
			        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
			        <?php endif; ?>
			        <?php endforeach; endif; else: echo "" ;endif; ?>
			    </select>
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 120px;">
				<select name="tjp" >
			        <option value="">订单类型</option>
			        <option value="t">淘宝订单</option>
			        <option value="j">京东订单</option>
			        <option value="p">拼多多订单</option>
			    </select>
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="text" name="keyword" placeholder="ID/订单ID/单号/商品名称" autocomplete="off" class="layui-input">
			</div>
		</div>
		 <div class="layui-inline">
		 	<div class="layui-input-inline">
		 		<button class="layui-btn layui-btn-sm sbtn" lay-submit="" lay-filter="searchsub" id="search"><i class="layui-icon"></i> 搜索</button>
		 		<button class="layui-btn layui-btn-sm layui-btn-danger"  id="export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 导出</button>
		 	</div>
		 </div>
	</form>
</blockquote>
<table id="tb1" lay-filter="_tb1"></table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.id }}">
</script>
<script type="text/html" id="bar">
	  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('del'); ?>?ids={{ d.id }}" >删除</a>
</script>
<script type="text/html" id="income_rate">{{d.income_rate}}%</script>
<script type="text/html" id="share_rate">{{d.share_rate}}%</script>
<script type="text/html" id="rj_rate">{{d.rj_rate}}%</script>
<script type="text/html" id="title">
	{{# if(d.tjp=='t'){ }}
	<span class="layui-text"><a href="https://item.taobao.com/item.html?id={{d.item_id}}" target="_blank" >{{d.title}}</a></span>
	{{# }else if(d.tjp=='j'){ }}
	<span class="layui-text"><a href="https://item.jd.com/{{d.item_id}}.html" target="_blank" >{{d.title}}</a></span>
	{{# }else if(d.tjp=='p'){ }}
	<span class="layui-text"><a href="http://mobile.yangkeduo.com/goods2.html?goods_id={{d.item_id}}" target="_blank" >{{d.title}}</a></span>
	{{# } }}
</script>
<script type="text/html" id="uid">
	<span class="layui-text"><a href="javascript:;"  class="show_userinfo"  data-title="【{{d.user_nickname}}】的用户信息" data-url="<?php echo url('user.index/info'); ?>?id={{d.uid}}" >[{{d.uid}}]{{d.user_nickname}}</a></span>
</script>

<script>
	layui.config({
		base: '/static/js/',
	});
	layui.use(['tool','laydate'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool,laydate=layui.laydate;
		tool.show_userinfo();
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('index'); ?>',
			limit:15,
			limits:[10,15,20,50,100],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-100',
			cols:[[
				{title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>',fixed:'left',templet:'#check',width:50},
				{title:'ID',field:'id',width:70},
				{title:'用户',field:'user_nickname',templet:'#uid'},
				{title:'单号',field:'trade_id',width:180},
				{title:'商品名称',field:'title',minWidth:300,templet:'#title'},
				{title:'订单状态',field:'order_status_text',width:90},
				{title:'订单类型',field:'tjp_text',width:90},
				{title:'实际付款',field:'pay_price',width:90,style:'color:#ff435b;'},
				{title:'数量',field:'item_num',width:60},
				{title:'收入比例',field:'income_rate',templet:'#income_rate',width:90},
				{title:'收入金额',field:'income_price',width:90},
				
				{title:'付款时间',field:'build_time_text',width:170},
				{title:'结算时间',field:'end_time_text',width:170},
				
				{title:'操作',fixed: 'right', width:70, align:'center', templet: '#bar'}

			]]
		});
		//搜索
		form.on('submit(searchsub)',function(data){
			var fields = $(data.form).serialize();
			tableobj.reload({
				where:data.field
				,page: {curr: 1 }
			});
			return false;
		})
		$('#kanjia_fafang').click(function(){
			layer.confirm('确定发放？', {
			  //btn: ['重要','奇葩'] //按钮
			  title:'非温馨提示'
			}, function(index){
				var index = layer.load(2);
				var url = '<?php echo url('order.taobao/kanjia_fafang'); ?>';
				$.post(url,function(ret){
					layer.close(index);
					if (ret.code==1) {
						tableobj.reload();
					}
					layer.msg(ret.msg);
				})
			});
		})
		//日期时间范围
		laydate.render({
		  elem: '#date'
		  ,type: 'datetime'
		  ,range: '~'
		  ,min:'2018-10-01 00:00:00'
		  ,max:'<?php echo date('Y-m-d 23:59:59',time()); ?>'
		});
		//export
		$('#export').click(function(){
			var url = '<?php echo url('export',['type'=>'order']); ?>?';
			var ids = $('.ids:checked').serialize();
			var form = $('form').serialize();
			location.href = url+'&'+form+'&'+ids;

			return false;
		});

	});
</script>

</html>