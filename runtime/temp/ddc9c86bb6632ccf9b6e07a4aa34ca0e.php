<?php /*a:2:{s:82:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/auth/adminuser/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<a href="javascript:;" class="layui-btn layui-btn-sm" id="add">
		<i class="layui-icon">&#xe608;</i> 添加
	</a>
	<button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
		<i class="layui-icon">&#xe640;</i> 删除
	</button>
</blockquote>

<table class="layui-table" lay-data="{ url:'<?php echo url('index'); ?>',limit:10,limits:[10,20,50,100],page:true}" lay-filter="datatable">
	<thead>
		<tr>
			<th lay-data="{fixed:'left',width:50,templet:'#check',title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>'}"></th>
			<th lay-data="{field:'uid', width:80, sort: true}">UID</th>
			<th lay-data="{field:'username',width:200}">用户名</th>
			<th lay-data="{field:'nickname',width:250}">昵称</th>
			<th lay-data="{field:'status',width:80}">状态</th>
			<th lay-data="{field:'login_time',width:180}">最后登录时间</th>
			<th lay-data="{fixed: 'right', align:'center', templet: '#bar'}">操作</th>
		</tr>
	</thead>
</table>




	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.uid }}">
</script>
<script type="text/html" id="bar">
<div class="layui-btn-group">
  <a class="layui-btn layui-btn-xs edit" data-url="<?php echo url('edit'); ?>?ids={{ d.uid }}" >编辑</a>
  <a class="layui-btn layui-btn-xs confirm_del" data-url="<?php echo url('del'); ?>?ids={{ d.uid }}" >删除</a>
</div>
</script>
<script>
	layui.use(['tool'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool;
		//添加
		$(document).on('click','#add,.edit',function(){
		    var url = '<?php echo url('add'); ?>',title = '添加管理员';
		    if($(this).hasClass('edit')){
		      url = $(this).data('url');
		      title = '编辑管理员';
		    }
		  	parent.layer.open({
		      title:title,
		      type: 2,
		      area: ['50%', '80%'],
		      fixed: false, //不固定
		      maxmin: true,
		      content: url,
		      shade:0,
		      id:'group_add'
		    });
		});

	});
</script>

</html>